import React, { Component } from "react";
import "bulma/css/bulma.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      tasks: [],
      laterTasks: [],
      completedTasks: [],

      modalTask: false,
      modalTask_Toggle: "",

      title: "",
      detail: "",
      complete: false,

      act: 0,
      index: 0,
      renderTasks: 1,
      navActive: "tasks",
    };
  }

  // make uniq id for every task
  uniqueId = () => {
    return "id-" + Math.random().toString(36).substr(2, 16);
  };

  //   TASKS
  modalTask = (modal) => {
    if (modal) {
      this.setState({
        modalTask_Toggle: "is-active",
        modalTask: modal,
      });
    } else {
      this.setState({ modalTask_Toggle: "", modalTask: modal });
    }
  };

  submitTask = (e) => {
    e.preventDefault();
    let { tasks, title, detail, complete, act, index } = this.state;

    if (act === 0) {
      //NEW task
      let task = {
        title,
        detail,
        complete,
        id: this.uniqueId(),
      };
      tasks.push(task);
    } else {
      //UPDATE task
      tasks[index].title = title;
      tasks[index].detail = detail;
    }

    this.setState({
      tasks,
      modalTask_Toggle: "",
      modalTask: false,
      // reset task or back to task
      title: "",
      detail: "",
      complete: false,
      act: 0,
    });
  };

  inputChange = (e) => {
    let { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  fRemove = (i) => {
    let { tasks } = this.state;
    tasks.splice(i, 1);
    this.setState({
      tasks,
      // reset
      title: "",
      detail: "",
      complete: false,
    });
  };

  fEdit = (i) => {
    let task = this.state.tasks[i];

    this.setState({
      title: task.title,
      detail: task.detail,

      modalTask_Toggle: "is-active",
      modalTask: true,

      act: 1,
      index: i,
    });
  };

  //   TASKS DONE
  taskDone = (i) => {
    let { laterTasks, tasks } = this.state;
    // insert done task
    laterTasks.push(tasks[i]);
    // delete task from tasks
    tasks.splice(i, 1);

    this.setState({
      laterTasks,
      tasks,
    });
  };

  removeDone = (i) => {
    let { laterTasks } = this.state;
    // delete laterTasks
    laterTasks.splice(i, 1);
    this.setState({
      laterTasks,
    });
  };

  unDone = (i) => {
    let { laterTasks, tasks } = this.state;
    // insert in tasks
    tasks.push(laterTasks[i]);

    // remove in laterTasks
    this.removeDone(i);
  };

  //  complete task
  tasksTocomplete = (i) => {
    let { completedTasks, tasks } = this.state;
    // insert done task
    completedTasks.push(tasks[i]);
    // delete task from tasks
    tasks.splice(i, 1);

    this.setState({
      completedTasks,
      tasks,
    });
  };

  doneTocomplete = (i, id) => {
    let { laterTasks, completedTasks } = this.state;

    if (laterTasks[i].complete === false) {
      // update task
      laterTasks[i].complete = true;
      // insert complete task
      completedTasks.push(laterTasks[i]);
    } else {
      // update task
      laterTasks[i].complete = false;
      // delete complete task
      completedTasks.splice(
        completedTasks.findIndex((e) => e.id === id),
        1
      );
    }

    this.setState({
      completedTasks,
      laterTasks,
    });
  };

  removecomplete = (i, id) => {
    let { completedTasks, tasks, laterTasks } = this.state;
    // delete in completedTasks
    completedTasks.splice(i, 1);

    // update tasks
    try {
      tasks[tasks.findIndex((e) => e.id === id)].complete = false;
    } catch (error) {
      console.log("not found in tasks");
    }

    // update tasksDone
    try {
      laterTasks[laterTasks.findIndex((e) => e.id === id)].complete = false;
    } catch (error) {
      console.log("not found in tasksDone");
    }

    this.setState({
      completedTasks,
      tasks,
      laterTasks,
    });
  };

  render() {
    let {
      modalTask,
      modalTask_Toggle,
      tasks,
      title,
      detail,
      laterTasks,
      completedTasks,
      renderTasks,
      navActive,
    } = this.state;

    return (
      <div className="App" style={{ paddingTop: 20 }}>
        {/* container */}
        <div className="container">
          {/* Top Button */}
          <div className="columns" style={{ position: "fixed" }}>
            <div className="column is-12">
              <div className="field has-addons">
                <p className="control">
                  <a
                    className="button is-link is-rounded"
                    onClick={() => this.modalTask(!modalTask)}
                  >
                    <span className="icon">
                      <i className="fa fa-plus" />
                    </span>
                    <span>New</span>
                  </a>
                </p>
                <p className="control">
                  <a
                    className={`button is-link ${
                      navActive === "tasks" ? "is-outlined" : ""
                    }`}
                    onClick={() => {
                      this.setState({
                        renderTasks: 1,
                        navActive: "tasks",
                      });
                    }}
                  >
                    <span className="icon">
                      <i className="fa fa-tasks" />
                    </span>
                    <span>Tasks ( {tasks.length} )</span>
                  </a>
                </p>
                <p className="control">
                  <a
                    className={`button is-link ${
                      navActive === "done" ? "is-outlined" : ""
                    }`}
                    onClick={() => {
                      this.setState({
                        renderTasks: 2,
                        navActive: "done",
                      });
                    }}
                  >
                    <span className="icon">
                      <i className="fa fa-sync" />
                    </span>
                    <span>Mark as Later ( {laterTasks.length} )</span>
                  </a>
                </p>
                <p className="control">
                  <a
                    className={`button is-link is-rounded ${
                      navActive === "complete" ? "is-outlined" : ""
                    }`}
                    onClick={() => {
                      this.setState({
                        renderTasks: 3,
                        navActive: "complete",
                      });
                    }}
                  >
                    <span className="icon">
                      <i className="fa fa-check" />
                    </span>
                    <span>Completed Task ( {completedTasks.length} )</span>
                  </a>
                </p>
              </div>
            </div>
          </div>

          {/* TODO LIST */}
          <div style={{ paddingTop: 60 }}>
            {renderTasks === 1 &&
              tasks.map((data, i) => (
                <div className="columns" key={i}>
                  <div className="column is-12">
                    <article className="media">
                      <div className="media-content">
                        <div className="content">
                          <p>
                            <strong>{data.title}</strong>
                            <br />
                            {data.detail}
                          </p>
                        </div>
                        <nav className="level is-mobile">
                          <div className="level-left">
                            <a
                              className="level-item"
                              onClick={() => this.tasksTocomplete(i, data.id)}
                            >
                              <span
                                className={`icon ${
                                  data.complete === true
                                    ? "has-text-danger"
                                    : ""
                                }`}
                              >
                                <i className="fa fa-lg fa-check"></i>
                              </span>
                            </a>
                            <a
                              className="level-item"
                              onClick={() => this.taskDone(i)}
                            >
                              <span className="icon">
                                <i className="fa fa-sync"></i>
                              </span>
                            </a>
                            <a
                              className="level-item"
                              onClick={() => this.fEdit(i)}
                            >
                              <span className="icon">
                                <i className="fa fa-lg fa-pencil-alt"></i>
                              </span>
                            </a>
                          </div>
                        </nav>
                      </div>
                      <div className="media-right">
                        <button
                          className="delete"
                          onClick={() => this.fRemove(i)}
                        ></button>
                      </div>
                    </article>
                  </div>
                </div>
              ))}

            {renderTasks === 2 &&
              laterTasks.map((data, i) => (
                <div className="columns" key={i}>
                  <div className="column is-12">
                    <article className="media">
                      <div className="media-content">
                        <div className="content">
                          <p>
                            <strong>{data.title}</strong>
                            <br />
                            {data.detail}
                          </p>
                        </div>
                        <nav className="level is-mobile">
                          <div className="level-left">
                            <a
                              className="level-item"
                              onClick={() => this.unDone(i)}
                            >
                              <span className="icon">
                                <i className="fa fa-lg fa-undo"></i>
                              </span>
                            </a>
                          </div>
                        </nav>
                      </div>
                      <div className="media-right">
                        <button
                          className="delete"
                          onClick={() => this.removeDone(i)}
                        ></button>
                      </div>
                    </article>
                  </div>
                </div>
              ))}

            {renderTasks === 3 &&
              completedTasks.map((data, i) => (
                <div className="columns" key={i}>
                  <div className="column is-12">
                    <article className="media">
                      <div className="media-content">
                        <div className="content">
                          <p>
                            <strong>{data.title}</strong>
                            <br />
                            {data.detail}
                          </p>
                        </div>
                      </div>
                      <div className="media-right">
                        <button
                          className="delete"
                          onClick={() => this.removecomplete(i, data.id)}
                        ></button>
                      </div>
                    </article>
                  </div>
                </div>
              ))}
          </div>
        </div>

        {/* MODAL - TASK FORM */}
        <div className={`modal ${modalTask_Toggle}`}>
          <div
            className="modal-background"
            onClick={() => this.modalTask(!modalTask)}
          ></div>
          <div className="modal-content">
            <form ref="myForm" className="myForm">
              <div className="field">
                <div className="control">
                  <label className="label" style={{ color: "#fff" }}>
                    Title
                  </label>
                  <input
                    className="input is-info"
                    type="text"
                    name="title"
                    value={title}
                    onChange={(e) => this.inputChange(e)}
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <label className="label" style={{ color: "#fff" }}>
                    Detail
                  </label>
                  <textarea
                    className="textarea is-info"
                    type="text"
                    placeholder="Info textarea"
                    name="detail"
                    value={detail}
                    onChange={(e) => this.inputChange(e)}
                  ></textarea>
                </div>
              </div>
              <button
                className="button is-info"
                onClick={(e) => this.submitTask(e)}
              >
                Save
              </button>
            </form>
          </div>
          <button
            className="modal-close is-large"
            aria-label="close"
            onClick={() => this.modalTask(!modalTask)}
          ></button>
        </div>
      </div>
    );
  }
}

export default App;
